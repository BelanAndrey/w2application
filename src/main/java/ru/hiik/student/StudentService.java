/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.student;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;



/**
 *
 * @author student
 */
@ApplicationScoped
public class StudentService {
    
   /**
    * Получение полного списка студентов
    * @return 
    */ 
   @Transactional 
   public List<Student> getAll()
   {       
       List<Student> students = Student.listAll();
       return students;
   }
    
   /**
    * Сохранение студента в базе данных 
    * 
    * @param student
    * @return 
    */
   @Transactional
   public Student save (Student student)
   {
       student.persistAndFlush();
       return student;
   }        
   
   @Transactional
   public Student delete (Student student)
   {
       student.delete();
       return student;
   }        
    
}
