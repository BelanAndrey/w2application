package ru.hiik.websocket;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;

import java.util.concurrent.ConcurrentHashMap;

import org.jboss.logging.Logger;

@ServerEndpoint("/ws/{username}")
@ApplicationScoped
public class StartWebSocket {

    private static final Logger LOG = Logger.getLogger(StartWebSocket.class);
    
    Map<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        sessions.put(username, session);
        LOG.info("Подключение пользователя: "+ username);
    }

    @OnClose
    public void onClose(Session session, @PathParam("username") String username) {
        sessions.remove(username);
        broadcast("Пользователь " + username + " вышел");
    }

    @OnError
    public void onError(Session session, @PathParam("username") String username, Throwable throwable) {
       // sessions.remove(username);
        LOG.error("Ошибка подключения: "+ throwable.getMessage());
        broadcast("Пользователь " + username + " Ошибка выхода: " + throwable);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("username") String username) {
        String info = username + ": " +message;
        LOG.info(info);
       
    }

    private void broadcast(String message) {
        sessions.values().forEach(s -> {
            s.getAsyncRemote().sendObject(message, result -> {
                if (result.getException() != null) {
                    System.out.println("Невозможно отправить сообщение: " + result.getException());
                }
            });
        });
    }
}
